<@requirement.CONSTRAINT 'traefik-mclogstorage' 'true' />

<@requirement.PARAM name='MCLOGSTORAGE_PUBLISHED_PROXY_PORT' type='port' value='7878' scope='global' />
<@requirement.PARAM name='MCLOGSTORAGE_PUBLISHED_PROXY_HTTPS_PORT' type='port' value='7743' required='false' scope='global' />
<@requirement.PARAM name='MCLOGSTORAGE_PUBLISHED_PROXY_ADMIN_PORT' type='port' value='7798' required='false' scope='global' />

<@requirement.PARAM name='TRAEFIK_MCLOGSTORAGE_ENTITY_ID' scope='global' required='false' />

<@swarm.SERVICE 'traefik-mclogstorage-${namespace}' 'registry.gitlab.com/equiron/sitemanager/traefik:1.7-alpine' "--logLevel=INFO --metrics.prometheus --docker --docker.swarmMode --docker.watch --api --constraints='tag==oms-mclogstorage-${namespace}'" 'global'>
  <@node.MANAGER />
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'traefik-mclogstorage' 'true' />
  <@service.PORT PARAMS.MCLOGSTORAGE_PUBLISHED_PROXY_PORT '80' 'host' />
  <@service.PORT PARAMS.MCLOGSTORAGE_PUBLISHED_PROXY_HTTPS_PORT '443' 'host' />
  <@service.PORT PARAMS.MCLOGSTORAGE_PUBLISHED_PROXY_ADMIN_PORT '8080' 'host' />
  <@service.CHECK_PATH ':8080/dashboard/' />
  <@service.ENV 'METRICS_ENDPOINT' ':8080/metrics' />
</@swarm.SERVICE>
