<@requirement.CONSTRAINT 'proxy-rem' 'true' />

<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />

<@requirement.PARAM name='TAG' value='latest-master' type='tag' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' value='' />
<@requirement.PARAM name='REM_JAVA_OPTS' value='-Xmx4g -Djava.awt.headless=true' scope='global' />

<@requirement.PARAM name='REM_ENTITY_ID' scope='global' required='false' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />

<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='PROXY_REM_PUBLISHED_PORT' type='port' required='false' description='Specify external port (for example 7071)' />


<@swarm.SERVICE 'proxy-rem-${namespace}' 'equironpnz/oms-rem:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.PORT PARAMS.PROXY_REM_PUBLISHED_PORT '7070' />

  <@service.CONSTRAINT 'proxy-rem' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.ENV 'JAVA_OPTS' PARAMS.REM_JAVA_OPTS />
  <@service.ENV 'SERVER_JMX_PORT' PARAMS.REM_JMX_PORT />
  <@service.ENV 'REM_ENTITY_ID' PARAMS.REM_ENTITY_ID />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' '80' />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />



<#if PARAMS.ENABLE_HTTPS == 'true'>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
  <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
  <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
<#else>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
</#if>

  <@service.LABEL 'traefik.port' '7070' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/heartbeat' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'proxy-rem-${namespace}' />

  <@service.CHECK_PATH ':7070/heartbeat' />
</@swarm.SERVICE>