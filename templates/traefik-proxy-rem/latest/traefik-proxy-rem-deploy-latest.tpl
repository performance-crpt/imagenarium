<@requirement.CONSTRAINT 'traefik-proxy-rem' 'true' />

<@requirement.PARAM name='REM_PUBLISHED_PROXY_PORT' type='port' value='7070' scope='global' />
<@requirement.PARAM name='REM_PUBLISHED_PROXY_HTTPS_PORT' type='port' value='7443' required='false' scope='global' />
<@requirement.PARAM name='REM_PUBLISHED_PROXY_ADMIN_PORT' type='port' value='7998' required='false' scope='global' />

<@requirement.PARAM name='TRAEFIK_PROXY_REM_ENTITY_ID' scope='global' required='false' />

<@swarm.SERVICE 'traefik-proxy-rem-${namespace}' 'registry.gitlab.com/equiron/sitemanager/traefik:1.7-alpine' "--logLevel=INFO --metrics.prometheus --docker --docker.swarmMode --docker.watch --api --constraints='tag==proxy-rem-${namespace}'" 'global'>
  <@node.MANAGER />
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'traefik-proxy-rem' 'true' />
  <@service.PORT PARAMS.REM_PUBLISHED_PROXY_PORT '80' 'host' />
  <@service.PORT PARAMS.REM_PUBLISHED_PROXY_HTTPS_PORT '443' 'host' />
  <@service.PORT PARAMS.REM_PUBLISHED_PROXY_ADMIN_PORT '7070' 'host' />
  <@service.CHECK_PATH ':8080/dashboard/' />
  <@service.ENV 'METRICS_ENDPOINT' ':8080/metrics' />
</@swarm.SERVICE>
