<@requirement.CONSTRAINT 'traefik-distribution-server' 'true' />

<@requirement.PARAM name='SERVER_PUBLISHED_PROXY_PORT' type='port' value='5080' scope='global' />
<@requirement.PARAM name='SERVER_PUBLISHED_PROXY_HTTPS_PORT' type='port' value='5443' required='false' scope='global' />
<@requirement.PARAM name='SERVER_PUBLISHED_PROXY_ADMIN_PORT' type='port' value='5081' required='false' scope='global' />

<@requirement.PARAM name='TRAEFIK_SERVER_ENTITY_ID' scope='global' required='false' />

<@swarm.SERVICE 'traefik-distribution-server-${namespace}' 'registry.gitlab.com/equiron/sitemanager/traefik:1.7-alpine' "--logLevel=INFO --metrics.prometheus --docker --docker.swarmMode --docker.watch --api --constraints='tag==distribution-server-${namespace}'" 'global'>
  <@node.MANAGER />
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'traefik-distribution-server' 'true' />
  <@service.PORT PARAMS.SERVER_PUBLISHED_PROXY_PORT '80' 'host' />
  <@service.PORT PARAMS.SERVER_PUBLISHED_PROXY_HTTPS_PORT '443' 'host' />
  <@service.PORT PARAMS.SERVER_PUBLISHED_PROXY_ADMIN_PORT '8080' 'host' />
  <@service.CHECK_PATH ':8080/dashboard/' />
  <@service.ENV 'METRICS_ENDPOINT' ':8080/metrics' />
</@swarm.SERVICE>
