<@requirement.CONSTRAINT 'traefik-transaction-server' 'true' />

<@requirement.PARAM name='GIS_PUBLISHED_PROXY_PORT' type='port' value='9050' scope='global' />
<@requirement.PARAM name='GIS_PUBLISHED_PROXY_HTTPS_PORT' type='port' value='9051' required='false' scope='global' />
<@requirement.PARAM name='GIS_PUBLISHED_PROXY_ADMIN_PORT' type='port' value='9052' required='false' scope='global' />

<@swarm.SERVICE 'traefik-transaction-server-${namespace}' 'registry.gitlab.com/equiron/sitemanager/traefik:1.7-alpine' "--logLevel=INFO --metrics.prometheus --docker --docker.swarmMode --docker.watch --api --constraints='tag==oms-transaction-server-${namespace}'" 'global'>
  <@node.MANAGER />
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'traefik-transaction-server' 'true' />
  <@service.PORT PARAMS.GIS_PUBLISHED_PROXY_PORT '80' 'host' />
  <@service.PORT PARAMS.GIS_PUBLISHED_PROXY_HTTPS_PORT '443' 'host' />
  <@service.PORT PARAMS.GIS_PUBLISHED_PROXY_ADMIN_PORT '8080' 'host' />
  <@service.CHECK_PATH ':8080/dashboard/' />
  <@service.ENV 'METRICS_ENDPOINT' ':8080/metrics' />
</@swarm.SERVICE>
