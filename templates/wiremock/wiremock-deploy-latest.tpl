<@requirement.CONSTRAINT 'wiremock' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' value='8080' type='port' required='false' />
<@requirement.PARAM name='CMD' required='false' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx1G -Xms1G' required='false' />

<@swarm.SERVICE  'wiremock-${namespace}' 'ovarb6/wiremock:2.35' PARAMS.CMD! >
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'wiremock' 'true' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.VOLUME '/home/wiremock' />
  <@service.VOLUME '/var/wiremock/extensions' />
  <@service.PORT PARAMS.PUBLISHED_PORT '8080' />
  <@service.CHECK_PORT '8080' />
</@swarm.SERVICE>