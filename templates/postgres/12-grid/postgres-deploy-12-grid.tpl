<@requirement.CONSTRAINT 'grid-${namespace}' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />
<@requirement.PARAM name='PMM_ENABLE' value='false' type='boolean' />

<@img.TASK 'postgres-${namespace}' 'imagenarium/postgresql:12.1' '-c max_connections=800 -c shared_buffers=1GB -c effective_io_concurrency=10'>
  <@img.NETWORK 'net-${namespace}' />
  <@img.PORT PARAMS.PUBLISHED_PORT '5432' 'host' />
  <@img.VOLUME '/var/lib/postgresql/data' />
  <@img.DNSRR />
  <@img.BIND '/sys/kernel/mm/transparent_hugepage' '/tph' />
  <@img.BIND '/dev/shm' '/dev/shm' />
  <@img.CONSTRAINT 'grid-${namespace}' 'true' />
  <@img.ENV 'POSTGRES_USER' 'postgres' />
  <@img.ENV 'POSTGRES_PASSWORD' 'postgres' />
  <@img.ENV 'POSTGRES_DB' 'postgres' />
  <@img.ENV 'APP_USER' 'app' />
  <@img.ENV 'APP_PASSWORD' 'app' />
  <@img.ENV 'NETWORK_NAME' 'net-${namespace}' />
  <@img.ENV 'PMM' PARAMS.PMM_ENABLE />
  <@img.CHECK_PORT '5432' />
</@img.TASK>
