<@requirement.CONSTRAINT 'grid-${namespace}' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />

<@swarm.SERVICE 'couchdb-${namespace}' 'imagenarium/couchdb:3.0.0'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.PORT PARAMS.PUBLISHED_PORT '5984' 'host' />
  <@service.VOLUME '/opt/couchdb/data' />
  <@service.VOLUME '/opt/couchdb/etc' />
  <@service.CONSTRAINT 'grid-${namespace}' 'true' />
  <@service.CHECK_PORT '5984' />
  <@service.ENV 'COUCHDB_USER' 'admin' />
  <@service.ENV 'COUCHDB_PASSWORD' 'PassWord123' />
</@swarm.SERVICE>