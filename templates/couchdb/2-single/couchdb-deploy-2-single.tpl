<@requirement.CONSTRAINT 'couchdb' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />
<@requirement.PARAM name='COUCHDB_PASSWORD' value='PassWord123' type='password' />

<@swarm.SERVICE 'couchdb-${namespace}' 'imagenarium/couchdb:2.3.1'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.PORT PARAMS.PUBLISHED_PORT '5984' 'host' />
  <@service.VOLUME '/opt/couchdb/data' />
  <@service.VOLUME '/opt/couchdb/etc' />
  <@service.CONSTRAINT 'couchdb' 'true' />
  <@service.CHECK_PORT '5984' />
  <@service.ENV 'COUCHDB_USER' 'admin' />
  <@service.ENV 'COUCHDB_PASSWORD' PARAMS.COUCHDB_PASSWORD />
</@swarm.SERVICE>