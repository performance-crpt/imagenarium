<@requirement.CONSTRAINT 'couchdb-proxy' 'true' />
<@requirement.CONSTRAINT 'couchdb-node' '1' />
<@requirement.CONSTRAINT 'couchdb-node' '2' />
<@requirement.CONSTRAINT 'couchdb-node' '3' />
<@requirement.CONSTRAINT 'couchdb-node' '4' />
<@requirement.CONSTRAINT 'couchdb-node' '5' />
<@requirement.CONSTRAINT 'couchdb-node' '6' />

<@requirement.PARAM name='COUCHDB_USER' value='admin' />
<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' />
<@requirement.PARAM name='PARTITIONS' value='8' />
<@requirement.PARAM name='REPLICAS' value='2' />

<@requirement.PARAM name='PROXY_BIND_PORT' value='5984' type='port' required='false' />
<@requirement.PARAM name='COUCHDB_PUBLISHED_PROXY_PORT' type='port' required='false' />
<@requirement.PARAM name='COUCHDB_PUBLISHED_PROXY_ADMIN_PORT' type='port' required='false' />
<@requirement.PARAM name='COUCHDB_PUBLISHED_SERVICE_PORT' type='port' required='false' scope='global' />

<#assign nodes = [] />
<#assign servers = [] />

<#list 1..6 as index>
  <#assign nodes += ['couchdb@couchdb-${index}-${namespace}'] />
  <#assign servers += ['http://couchdb-${index}-${namespace}:5984'] />
</#list>

<#list 1..6 as index>
  <@swarm.SERVICE 'couchdb-${index}-${namespace}' 'imagenarium/couchdb-cluster:2.3.2'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.PORT PARAMS.COUCHDB_PUBLISHED_SERVICE_PORT '5984' 'host' />
    <@service.VOLUME '/opt/couchdb/data' />
    <@service.VOLUME '/opt/couchdb/etc' />
    <@service.CONSTRAINT 'couchdb-node' '${index}' />
    <@service.ENV 'COUCHDB_USER' PARAMS.COUCHDB_USER />
    <@service.ENV 'COUCHDB_PASSWORD' PARAMS.COUCHDB_PASSWORD />
    <@service.ENV 'NODENAME' 'couchdb-${index}-${namespace}' />
    <@service.ENV 'SEEDLIST' nodes?join(',') />
    <@service.ENV 'PARTITIONS' PARAMS.PARTITIONS />
    <@service.ENV 'REPLICAS' PARAMS.REPLICAS />
  </@swarm.SERVICE>
</#list>

<@swarm.SERVICE 'couchdb-${namespace}' 'imagenarium/traefik:2' '' 'global'>
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'couchdb-proxy' 'true' />
  <@service.PORT PARAMS.COUCHDB_PUBLISHED_PROXY_PORT PARAMS.PROXY_BIND_PORT 'host' />
  <@service.PORT PARAMS.COUCHDB_PUBLISHED_PROXY_ADMIN_PORT '8080' 'host' />
  <@service.ENV 'SERVERS' servers?join(',') />
  <@service.ENV 'PORT' PARAMS.PROXY_BIND_PORT />
  <@service.CHECK_PATH ':${PARAMS.PROXY_BIND_PORT}' />
</@swarm.SERVICE>

<@docker.CONTAINER 'couchdb-checker-${namespace}' 'imagenarium/couchdb-cluster:2.3.2'>
  <@container.ENTRY '/check_cluster.sh' />
  <@container.NETWORK 'net-${namespace}' />
  <@container.EPHEMERAL />
  <@service.ENV 'COUCHDB_HOST' 'http://${PARAMS.COUCHDB_USER}:${PARAMS.COUCHDB_PASSWORD}@couchdb-${namespace}:${PARAMS.PROXY_BIND_PORT}' />
</@docker.CONTAINER>
