<@requirement.CONSTRAINT 'traefik-sso' 'true' />

<@requirement.PARAM name='SSO_PUBLISHED_PROXY_PORT' type='port' value='7080' scope='global' />
<@requirement.PARAM name='SSO_PUBLISHED_PROXY_HTTPS_PORT' type='port' value='7483' required='false' scope='global' />
<@requirement.PARAM name='SSO_PUBLISHED_PROXY_ADMIN_PORT' type='port' value='7988' required='false' scope='global' />

<@requirement.PARAM name='TRAEFIK_SSO_ENTITY_ID' scope='global' required='false' />

<@swarm.SERVICE 'traefik-sso-${namespace}' 'registry.gitlab.com/equiron/sitemanager/traefik:1.7-alpine' "--logLevel=INFO --metrics.prometheus --docker --docker.swarmMode --docker.watch --api --constraints='tag==oms-sso-${namespace}'" 'global'>
  <@node.MANAGER />
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'traefik-sso' 'true' />
  <@service.PORT PARAMS.SSO_PUBLISHED_PROXY_PORT '80' 'host' />
  <@service.PORT PARAMS.SSO_PUBLISHED_PROXY_HTTPS_PORT '443' 'host' />
  <@service.PORT PARAMS.SSO_PUBLISHED_PROXY_ADMIN_PORT '8080' 'host' />
  <@service.CHECK_PATH ':8080/dashboard/' />
  <@service.ENV 'METRICS_ENDPOINT' ':8080/metrics' />
</@swarm.SERVICE>
