<@requirement.CONSTRAINT 'oms-mclogstorage' 'true' />

<@requirement.PARAM name='SERVER_JMX_PORT' value='' required='false' type='port' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' required='true' />
<@requirement.PARAM name='COUCHDB_PORT' values='80,5984' value='5984' type='select' required='false' />
<@requirement.PARAM name='SETTINGS_IS_MDLP_URL' scope='global' required='true' />
<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='TAG' value='latest-master' type='tag' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' value='' />
<@requirement.PARAM name='SERVER_JAVA_OPTS' value='-Xmx2g -Djava.awt.headless=true' scope='global' />

<@requirement.PARAM name='MC_LOG_STORAGE_ENTITY_ID' scope='global' required='false' />

<@swarm.SERVICE 'oms-mclogstorage-${namespace}' 'equironpnz/oms-mclogstorage:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.SERVER_PUBLISHED_PORT '8083' />
  <@service.PORT PARAMS.SERVER_JMX_PORT PARAMS.SERVER_JMX_PORT />
  <@service.CONSTRAINT 'oms-mclogstorage' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.ENV 'JAVA_OPTS' PARAMS.SERVER_JAVA_OPTS />
  <@service.ENV 'SERVER_JMX_PORT' PARAMS.SERVER_JMX_PORT />
  <@service.ENV 'MC_LOG_STORAGE_ENTITY_ID' PARAMS.MC_LOG_STORAGE_ENTITY_ID />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.COUCHDB_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' PARAMS.SETTINGS_IS_MDLP_URL />

  <@service.LABEL 'traefik.port' '8083' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/info' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'oms-mclogstorage-${namespace}' />
  <#if PARAMS.ENABLE_HTTPS == 'true'>
    <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
    <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
    <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
  <#else>
    <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
  </#if>

  <@service.CHECK_PATH ':8083/info' />
</@swarm.SERVICE>
