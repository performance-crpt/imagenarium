<@requirement.CONSTRAINT 'wiremockgui' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' value='8080' type='port' required='false' />
<@requirement.PARAM name='CMD' required='false' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx1G -Xms1G' required='false' />

<@swarm.SERVICE 'wiremockgui-${namespace}' 'registry.gitlab.com/performance-crpt/imagenarium/wiremockgui:latest' PARAMS.CMD! >
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'wiremockgui' 'true' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.VOLUME '/home/wiremock' />
  <@service.VOLUME '/var/wiremock/extensions' />
  <@service.PORT PARAMS.PUBLISHED_PORT '8080' />
  <@service.CHECK_PORT '8080' />
</@swarm.SERVICE>
