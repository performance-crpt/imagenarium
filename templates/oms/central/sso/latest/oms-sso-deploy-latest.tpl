<@requirement.CONSTRAINT 'oms-sso' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='5984' type='select' required='false' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-sso' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />
<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='OMS_SSO_ENTITY_ID' scope='global' required='false' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<#if environment == 'INTEGRATION' || environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3' || environment == 'PRE-PROD' || environment == 'TESTUOT'>

<#else>
  <#assign unknownEnvironment = true />
</#if>

<@swarm.SERVICE 'oms-sso-${namespace}' 'equironpnz/oms-sso:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.CONSTRAINT 'oms-sso' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-sso-${namespace}:4560' />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
  <@service.ENV 'SSO_NAME' 'SSO-${environment}-${namespace}' />

  <@service.ENV 'OMS_SSO_ENTITY_ID' PARAMS.OMS_SSO_ENTITY_ID />
  
<#if unknownEnvironment??>
  <@service.ENV 'AUTO_APPROVE' 'true' />
  
  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  
  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'TESTUOT'>
  <@service.ENV 'AUTO_APPROVE' 'true' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'PRE-PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'HOTFIX'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'INTEGRATION'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'INTEGRATION_LP'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'PROD2'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if environment == 'PROD3'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
</#if>

<#if PARAMS.ENABLE_HTTPS == 'true'>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
  <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
  <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
<#else>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
</#if>

  <@service.LABEL 'traefik.port' '9080' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/version' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'oms-sso-${namespace}' />

  <@service.CHECK_PATH ':9080/version' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-sso-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-sso' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-sso-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'sso' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>