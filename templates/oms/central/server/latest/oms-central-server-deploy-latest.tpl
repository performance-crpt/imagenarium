<@requirement.CONSTRAINT 'oms-central-server' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='5984' type='select' required='false' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-central-server' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />
<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='OMS_CENTRAL_SERVER_ENTITY_ID' scope='global' required='false' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<#if environment == 'INTEGRATION' || environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3' || environment == 'PRE-PROD' || environment == 'TESTUOT'>

<#else>
  <#assign unknownEnvironment = true />
</#if>

<@swarm.SERVICE 'oms-central-server-${namespace}' 'equironpnz/oms-central-server:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.CONSTRAINT 'oms-central-server' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-central-server-${namespace}:4560' />
  <@service.ENV 'OMS_CLOUD_URL' 'http://oms-${namespace}:8080/' />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
  <@service.ENV 'CENTRAL_SERVER_NAME' 'CENTRAL_SERVER-${environment}-${namespace}' />

  <@service.ENV 'OMS_CENTRAL_SERVER_ENTITY_ID' PARAMS.OMS_CENTRAL_SERVER_ENTITY_ID />
  
<#if unknownEnvironment??>
  <@service.ENV 'AUTO_APPROVE' 'true' />
  
  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  
  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>

<#if environment == 'TESTUOT'>
  <@service.ENV 'AUTO_APPROVE' 'true' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>

<#if environment == 'PRE-PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>
  
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://it.mdlp.crpt.ru/' />
  
  <#--  Use omslet settings
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  -->
</#if>

<#if environment == 'LOAD'>
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>

<#if environment == 'PRE-PROD_F'>

  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <#--  Use omslet settings
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  -->
</#if>

<#if environment == 'HOTFIX'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>
  
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  
  <#--  Use omslet settings
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  -->
</#if>

<#if environment == 'INTEGRATION'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/'/>
  
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  
  <#--  Use omslet settings
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' '' />
  
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  -->
</#if>

<#if environment == 'INTEGRATION_LP'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/'/>
  
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  
  <#--  Use omslet settings
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://demo.lp.crpt.tech' />
  
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  -->
</#if>

<#if environment == 'CENTRAL-PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />
  
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/'/>
  
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://mdlp.crpt.ru/' />
  
  <#--  Use omslet settings
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://prod.lp.crpt.tech' />
  
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://mdlp.crpt.ru'/>
  -->
  
</#if>

<#if PARAMS.ENABLE_HTTPS == 'true'>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
  <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
  <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
<#else>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
</#if>

  <@service.LABEL 'traefik.port' '9090' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/version' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'oms-central-server-${namespace}' />

  <@service.CHECK_PATH ':9090/version' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-central-server-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-central-server' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-central-server-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'centralserver' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>