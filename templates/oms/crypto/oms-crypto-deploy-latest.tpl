<@requirement.CONSTRAINT 'oms-crypto' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />

<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-crypto' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />

<@requirement.PARAM name='OMS_INTEGRATION_CRYPTO_ENTITY_ID'	scope='global' required='false' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<#if environment == 'INTEGRATION' || environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3' || environment == 'FUNCTIONAL' || environment == 'PRE-PROD' || environment == 'TESTUOT'>

<#else>
  <#assign unknownEnvironment = true />
</#if>

<@swarm.SERVICE 'oms-crypto-${namespace}' 'equironpnz/oms-crypto:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.CONSTRAINT 'oms-crypto' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-crypto-${namespace}:4560' />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
  <@service.ENV 'SERVER_NAME' 'SERVER-${environment}-${namespace}' />

  <@service.ENV 'OMS_INTEGRATION_CRYPTO_ENTITY_ID' PARAMS.OMS_INTEGRATION_CRYPTO_ENTITY_ID />  
<#if unknownEnvironment??>
  <@service.ENV 'JAVA_OPTS' '-Xmx4g -Djava.awt.headless=true' />
  <@service.ENV 'AUTO_APPROVE' 'true' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
</#if>

<#if environment == 'FUNCTIONAL'>
  <@service.ENV 'JAVA_OPTS' '-Xmx2g -Djava.awt.headless=true' />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />
</#if>

<#if environment == 'TESTUOT'>
  <@service.ENV 'JAVA_OPTS' '-Xmx2g -Djava.awt.headless=true' />
  <@service.ENV 'AUTO_APPROVE' 'true' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
</#if>

<#if environment == 'PRE-PROD'>
  <@service.ENV 'JAVA_OPTS' '-Xmx2g -Djava.awt.headless=true' />
  <@service.ENV 'AUTO_APPROVE' 'false' />
  
  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />
</#if>

<#if environment == 'INTEGRATION'>
  <@service.ENV 'JAVA_OPTS' '-Xmx4g -Djava.awt.headless=true' />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />
</#if>

<#if environment == 'PROD'>
  <@service.ENV 'JAVA_OPTS' '-Xmx8g -Djava.awt.headless=true' />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://192.168.51.66/' />
</#if>

<#if environment == 'PROD2'>
  <@service.ENV 'JAVA_OPTS' '-Xmx8g -Djava.awt.headless=true' />

  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://192.168.51.66/' />
</#if>

<#if environment == 'PROD3'>
  <@service.ENV 'JAVA_OPTS' '-Xmx8g -Djava.awt.headless=true' />

  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://192.168.51.66/' />
</#if>

</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-crypto-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-crypto' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-crypto-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'crypto' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>