<@requirement.CONSTRAINT 'distribution-client-cem' 'true' />

<@requirement.PARAM name='DISTRIBUTION_SERVER_URL' scope='global' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' type='tag' filter='oms-distrib-cem' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />
<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='DISTRIBUTION_CLIENT_ENTITY_ID' scope='global' required='false' />
<@requirement.PARAM name='DIGITAL_SIGNATURE_REQUIRED' value='true' type='boolean' required='true' scope='global' />
<@requirement.PARAM name='SWAGGER_WEB_API' value='false' type='boolean' required='true' scope='global' />

<#if environment == 'INTEGRATION' || environment == 'INTEGRATION_LP' || environment == 'PROD' || environment == 'PROD2' || environment == 'PRE-PROD' || environment == 'TESTUOT'>
<#else>
  <#assign unknownEnvironment = true />
</#if>

<@swarm.SERVICE 'distribution-client-cem-ui-${namespace}' 'registry.gitlab.com/equiron/oms-distrib-ui/frontnend:1.2.0t'>
  <@service.CONSTRAINT 'distribution-client-cem' 'true' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CHECK_PORT '3000' />
  <@service.ENV 'API_URL' 'http://distribution-client-cem-${namespace}:6080' />
</@swarm.SERVICE>

<@swarm.SERVICE 'distribution-client-cem-${namespace}' 'equironpnz/oms-distrib-cem:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.CONSTRAINT 'distribution-client-cem' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.ENV 'SERVER_URL' PARAMS.DISTRIBUTION_SERVER_URL />

  <@service.ENV 'postgresIp' 'postgres-${namespace}' />
  <@service.ENV 'postgresPort' '5432' />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
  <@service.ENV 'UI_URL' 'http://distribution-client-cem-ui-${namespace}:3000' />

  <@service.ENV 'DISTRIBUTION_CLIENT_ENTITY_ID' PARAMS.DISTRIBUTION_client_ENTITY_ID />
  <@service.ENV 'DIGITAL_SIGNATURE_REQUIRED' PARAMS.DIGITAL_SIGNATURE_REQUIRED />
  <@service.ENV 'SWAGGER_WEB_API' PARAMS.SWAGGER_WEB_API />
  
<#if unknownEnvironment??>

</#if>

<#if environment == 'TESTUOT'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD />-->
</#if>

<#if environment == 'PRE-PROD'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if environment == 'INTEGRATION'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if environment == 'INTEGRATION_LP'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if environment == 'PROD' || environment == 'PROD2'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if PARAMS.ENABLE_HTTPS == 'true'>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
  <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
  <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
<#else>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
</#if>

  <@service.LABEL 'traefik.port' '6080' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/swagger-ui.html' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'distribution-client-cem-${namespace}' />

  <@service.CHECK_PATH ':6080/swagger-ui.html' />
</@swarm.SERVICE>
