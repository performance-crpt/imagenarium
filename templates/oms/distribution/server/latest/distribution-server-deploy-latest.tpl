<@requirement.CONSTRAINT 'distribution-server' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true -Dcom.sun.security.enableCRLDP=true -Docsp.enable=true' scope='global' />
<@requirement.PARAM name='TAG' type='tag' filter='oms-distrib-server' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />
<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />
<@requirement.PARAM name='DISTRIBUTION_SERVER_ENTITY_ID' scope='global' required='false' />

<@requirement.PARAM name='ENABLE_TEST_CERT' value='false' type='boolean' scope='global' />

<@requirement.PARAM name='DCS_TOKEN' value='2cecc8fb-fb47-4c8a-af3d-d34c1ead8c4f' required='true' scope='global' />

<#if environment == 'INTEGRATION' || environment == 'INTEGRATION_LP' || environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3' || environment == 'PRE-PROD' || environment == 'TESTUOT'>
<#else>
  <#assign unknownEnvironment = true />
</#if>

<@swarm.SERVICE 'distribution-server-${namespace}' 'equironpnz/oms-distrib-server:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.CONSTRAINT 'distribution-server' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  
  <@service.ENV 'LOGSTASH_HOST' 'logstash-distributionserver-${namespace}:4560' />

  <@service.ENV 'DISTRIBUTION_SERVER_ENTITY_ID' PARAMS.DISTRIBUTION_SERVER_ENTITY_ID />
  
  <@service.ENV 'ENABLE_TEST_CERT' PARAMS.ENABLE_TEST_CERT />
  
  <@service.ENV 'DCS_TOKEN' PARAMS.DCS_TOKEN />
<#if unknownEnvironment??>

</#if>

<#if environment == 'TESTUOT'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD />-->
</#if>

<#if environment == 'PRE-PROD'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if environment == 'INTEGRATION'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if environment == 'INTEGRATION_LP'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3'>
<#-- 
  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' 'true' />
  <@service.ENV 'CENTRAL_DB_DB_IP' 'couchdb-${namespace}' />
  <@service.ENV 'CENTRAL_DB_DB_PORT' '80' />
  <@service.ENV 'CENTRAL_DB_USER' 'admin' />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.COUCHDB_PASSWORD /> -->
</#if>

<#if PARAMS.ENABLE_HTTPS == 'true'>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
  <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
  <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
<#else>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
</#if>

  <@service.LABEL 'traefik.port' '5080' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '1073741824' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '1073741824' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '209715200' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '209715200' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/swagger-ui.html' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'distribution-server-${namespace}' />

  <@service.CHECK_PATH ':5080/swagger-ui.html' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-distributionserver-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'distribution-server' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-distributionserver-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'distributionserver' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>