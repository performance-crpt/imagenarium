<@requirement.NAMESPACE 'grid-router' />

<@requirement.CONSTRAINT 'oms-router' 'true' />

<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-router' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />

<@requirement.PARAM name='ROUTER_PUBLISHED_PORT' value='9999' required='false' scope='global' />
<@requirement.PARAM name='JMX_PUBLISHED_PORT' value='19010' required='false' scope='global' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<@swarm.SERVICE 'oms-router-${namespace}' 'equironpnz/oms-router:${PARAMS.TAG}' '' 'global'>
  <@service.CONSTRAINT 'oms-router' 'true' />
  <@node.MANAGER />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />  
  <@service.DNSRR />  
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.PORT PARAMS.ROUTER_PUBLISHED_PORT '8811' 'host' />
  <@service.PORT PARAMS.JMX_PUBLISHED_PORT '19010' 'host' />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-omsrouter-${namespace}:4560' />
  <@service.CHECK_PORT '8811' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-omsrouter-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-router' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' '-Xms1g -Xmx1g -Dnetworkaddress.cache.ttl=10' />
    <@service.ENV 'NODE_NAME' 'logstash-omsrouter-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'omsrouter' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>
