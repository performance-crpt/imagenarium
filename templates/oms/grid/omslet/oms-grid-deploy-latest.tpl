<@requirement.CONSTRAINT 'grid-${namespace}' 'true' />

<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-app' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true -Dcom.sun.security.enableCRLDP=true -Docsp.enable=true' scope='global' />

<@requirement.PARAM name='OMS_PUBLISHED_PORT' required='false' />
<@requirement.PARAM name='PROFILER_PORT' required='false' type='port' />
<@requirement.PARAM name='OMS_WORKER_THREADS' value='400' required='false' />
<@requirement.PARAM name='OMS_PG_POOL_SIZE' value='600' required='false' />

<@requirement.PARAM name='OMS_TRANSACTION_API_URL' value='' required='false' scope='global' />

<@requirement.PARAM name='OMS_IN_MEMORY' type='boolean' value='false' scope='global' />
<@requirement.PARAM name='MAX_CODES_IN_MEMORY_STORAGE_SIZE_MB' value='100' required='false' />
<@requirement.PARAM name='BUFFER_FORCE_OUT_TIMEOUT_MIN' value='1440' required='false' />

<@requirement.PARAM name='OMS_AUTO_APPROVE' type='boolean' value='false' scope='global' />
<@requirement.PARAM name='STANDALONE' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='ENABLE_TEST_CERT' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='ENABLE_PROFILER' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='SETTINGS_ANTI_DDOS_DISABLE' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='DISABLE_SIGNIN' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='OMSLET_ID' scope='global' required='true' regexp='\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b' />
<@requirement.PARAM name='REM_PROXY_TOKEN' scope='global' required='false' />

<@requirement.PARAM name='CENTRAL_DB_ENABLE_REPLICATION' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PROTOCOL' values='http,https' value='http' type='select' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_IP' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PORT' value='5984' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_USER' value='admin' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PASSWORD' type='password' required='false' scope='global' />

<@requirement.PARAM name='LS_JAVA_OPTS' value='-Xms1g -Xmx1g -Dnetworkaddress.cache.ttl=10' />
<@requirement.PARAM name='ES_URL' required='false' />
<@requirement.PARAM name='ES_USERNAME' required='false' value='suz_logs' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' />
<@requirement.PARAM name='INDEX_NAME' value='omsapp' description='Index name prefix (no dashes please!)' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' />
<@requirement.PARAM name='DELETE_DAYS' value='30' />

<@requirement.PARAM name='OMS_ROUTER_NAMESPACE' value='grid-router' scope='global' />
<@requirement.PARAM name='OMS_HOME_URL' required='false' scope='global' />

<#if environment == 'HOTFIX' || environment == 'INTEGRATION' || environment == 'INTEGRATION_LP' || environment == 'PROD' || environment == 'PROD2' || environment == 'PRE-PROD' || environment == 'PRE-PROD_F' || environment == 'TESTUOT' || environment == 'LOAD'>

<#else>
  <#assign unknownEnvironment = true />
</#if>

<@requirement.PARAM name='BUFFER_PENDING_REJECT_TIMEOUT_MIN' required='false' value='30' />
<@requirement.PARAM name='BUFFER_ACTIVE_REJECT_TIMEOUT_MIN'  required='false' value='5' />
<@requirement.PARAM name='POOLS_CHECKER_TIMEOUT_SEC'         required='false' value='30' />

<@requirement.PARAM name='POOLS_CHECKER_SCHEDULER_SIZE'   required='false' value='8' />
<@requirement.PARAM name='ORDERS_APPROVER_SCHEDULER_SIZE' required='false' value='4' />
<@requirement.PARAM name='BUFFERS_FILLER_SCHEDULER_SIZE'  required='false' value='4' />

<@swarm.SERVICE 'oms-ui-${namespace}' 'registry.gitlab.com/equiron/oms-ui:1.14.23LP'>
  <@service.CONSTRAINT 'grid-${namespace}' 'true' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CHECK_PORT '3000' />
  <@service.ENV 'API_URL' 'http://oms-${namespace}:8080' />
  <@service.ENV 'DISABLE_SIGNIN' PARAMS.DISABLE_SIGNIN />
</@swarm.SERVICE>

<@swarm.SERVICE 'oms-${namespace}' 'registry.gitlab.com/equiron/sitemanager/oms-app:${PARAMS.TAG}'>
  <@service.PORT PARAMS.PROFILER_PORT '9955' 'host' />

  <@service.CONSTRAINT 'grid-${namespace}' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.NETWORK 'net-${PARAMS.OMS_ROUTER_NAMESPACE}' />
  <@service.PORT PARAMS.OMS_PUBLISHED_PORT '8080' 'host' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  
  <@service.LABEL 'omsRouter' PARAMS.OMS_ROUTER_NAMESPACE />
  
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.ENV 'IN_MEMORY' PARAMS.OMS_IN_MEMORY />
  <@service.ENV 'MAX_CODES_IN_MEMORY_STORAGE_SIZE_MB' PARAMS.MAX_CODES_IN_MEMORY_STORAGE_SIZE_MB />
  <@service.ENV 'BUFFER_FORCE_OUT_TIMEOUT_MIN' PARAMS.BUFFER_FORCE_OUT_TIMEOUT_MIN />

  <@service.ENV 'postgresIp' 'postgres-${namespace}' />
  <@service.ENV 'postgresPort' '5432' />
  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' '5984' />
  <@service.ENV 'couchDbPassword' 'PassWord123' />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-oms-${namespace}:4560' />

  <@service.ENV 'OMSLET_ID' PARAMS.OMSLET_ID />
  <@service.ENV 'UI_URL' 'http://oms-ui-${namespace}:3000' />
  <@service.ENV 'OMS_TRANSACTION_API_URL' PARAMS.OMS_TRANSACTION_API_URL />


  <@service.ENV 'REM_PROXY_TOKEN' PARAMS.REM_PROXY_TOKEN />
  <@service.ENV 'OMS_SERVER_URL' 'http://oms-server-${namespace}:9090/' />

  <@service.ENV 'SPRING_PROFILES_ACTIVE' 'TOBACCO,PHARMA,LIGHT,SHOES,MILK,TIRES,PHOTO,PERFUM,BICYCLE,WHEELCHAIRS,OTP${(PARAMS.STANDALONE == "true")?then(",STANDALONE", "")}' />

  <@service.ENV 'ENABLE_PROFILER' PARAMS.ENABLE_PROFILER />
  <@service.ENV 'OMS_WORKER_THREADS' PARAMS.OMS_WORKER_THREADS />
  <@service.ENV 'OMS_PG_POOL_SIZE' PARAMS.OMS_PG_POOL_SIZE />
  <@service.ENV 'BUFFER_PENDING_REJECT_TIMEOUT_MIN' PARAMS.BUFFER_PENDING_REJECT_TIMEOUT_MIN />
  <@service.ENV 'BUFFER_ACTIVE_REJECT_TIMEOUT_MIN' PARAMS.BUFFER_ACTIVE_REJECT_TIMEOUT_MIN />
  <@service.ENV 'POOLS_CHECKER_TIMEOUT_SEC' PARAMS.POOLS_CHECKER_TIMEOUT_SEC />
  <@service.ENV 'POOLS_CHECKER_SCHEDULER_SIZE' PARAMS.POOLS_CHECKER_SCHEDULER_SIZE />
  <@service.ENV 'ORDERS_APPROVER_SCHEDULER_SIZE' PARAMS.ORDERS_APPROVER_SCHEDULER_SIZE />
  <@service.ENV 'BUFFERS_FILLER_SCHEDULER_SIZE' PARAMS.BUFFERS_FILLER_SCHEDULER_SIZE />
  <@service.ENV 'BUFFER_SIZE' PARAMS.BUFFER_SIZE />
  <@service.ENV 'CLOUD_OMS_NAME' 'OMS-${environment}-${namespace}' />

  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' PARAMS.CENTRAL_DB_ENABLE_REPLICATION />
  <@service.ENV 'CENTRAL_DB_PROTOCOL' PARAMS.CENTRAL_DB_PROTOCOL />
  <@service.ENV 'CENTRAL_DB_IP' PARAMS.CENTRAL_DB_IP />
  <@service.ENV 'CENTRAL_DB_PORT' PARAMS.CENTRAL_DB_PORT />
  <@service.ENV 'CENTRAL_DB_USER' PARAMS.CENTRAL_DB_USER />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.CENTRAL_DB_PASSWORD />

  <@service.ENV 'SETTINGS_IS_MP_VALIDATION_SOURCE' 'KM_ORDERS' />
  <@service.ENV 'SETTINGS_IS_MP_OTP_VALIDATION_SOURCE' 'KM_ORDERS' />

<#if unknownEnvironment??>
  <@service.ENV 'AUTO_APPROVE' PARAMS.OMS_AUTO_APPROVE />
  <@service.ENV 'OMS_HOME_URL' PARAMS.OMS_HOME_URL />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>


<#if environment == 'PRE-PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://omspreprod.crptech.ru:12001/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://it.mdlp.crpt.ru'/>
</#if>

<#if environment == 'LOAD'>
  <@service.ENV 'AUTO_APPROVE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>

<#if environment == 'PRE-PROD_F'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://omspreprod.crptech.ru:12001/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
</#if>

<#if environment == 'HOTFIX'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://omshotfix.crptech.ru:12011/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  
  <@service.ENV 'TEST_MODE' 'true' />
</#if>

<#if environment == 'INTEGRATION'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://intpharma.crpt.ru:12001/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' '' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/'/>

  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'INTEGRATION_LP'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://intuot.crpt.ru:12011/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://demo.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/'/>

  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://suz.crpt.ru/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' '' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/'/>
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' ''/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://mdlp.crpt.ru'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'PROD2'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://suz2.crpt.ru/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://prod.lp.crpt.tech' />

  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/'/>
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'TESTUOT'>
  <@service.ENV 'AUTO_APPROVE' 'true' />
  <@service.ENV 'OMS_HOME_URL' 'http://testuot.crpt.ru:45676/' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>

  <@service.CHECK_PATH ':8080/webapi/v1/version' />

  <@service.ENV 'ENABLE_TEST_CERT' PARAMS.ENABLE_TEST_CERT />
  <@service.ENV 'SETTINGS_ANTI_DDOS_DISABLE' PARAMS.SETTINGS_ANTI_DDOS_DISABLE />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-oms-${namespace}' 'imagenarium/logstash-oms:6.8'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'grid-${namespace}' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_USERNAME' PARAMS.ES_USERNAME />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-oms-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' PARAMS.INDEX_NAME />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>
