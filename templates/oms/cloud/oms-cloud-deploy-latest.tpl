<@requirement.CONSTRAINT 'oms-checkqr' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='COUCHDB_PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true -Dcom.sun.security.enableCRLDP=true -Docsp.enable=true' scope='global' />
<@requirement.PARAM name='PROFILER_PORT' value='9955' required='false' type='port' />
<@requirement.PARAM name='OMS_WORKER_THREADS' value='1000' required='false' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-app' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' value='' />
<@requirement.PARAM name='OMS_AUTO_APPROVE' type='boolean' value='false' />
<@requirement.PARAM name='ENABLE_TEST_CERT' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='ENABLE_PROFILER' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='OMS_HOME_URL' required='true' scope='global' />
<@requirement.PARAM name='OMS_ROUTER_NAMESPACE' value='grid-router' scope='global' />

<@requirement.PARAM name='OMS_IN_MEMORY' type='boolean' value='false' scope='global' />
<@requirement.PARAM name='MAX_CODES_IN_MEMORY_STORAGE_SIZE_MB' value='100' required='false' />
<@requirement.PARAM name='BUFFER_FORCE_OUT_TIMEOUT_MIN' value='1440' required='false' />

<@requirement.PARAM name='OMS_CLOUD_ENTITY_ID' scope='global' required='false' required='false' />
<@requirement.PARAM name='OMSLET_ID' scope='global' required='true'/>
<@requirement.PARAM name='REM_PROXY_TOKEN' scope='global' required='false' />

<@requirement.PARAM name='DISABLE_SIGNIN' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='CENTRAL_DB_ENABLE_REPLICATION' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_IP' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PORT' value='5984' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_USER' value='admin' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PASSWORD' value='' type='password' required='false' scope='global' />

<@requirement.PARAM name='STANDALONE' value='false' type='boolean' scope='global' />

<@requirement.PARAM name='DISABLE_GIS_TRANSACTION' value='false' type='boolean'/>
<@requirement.PARAM name='BILLING_SERVER_URL' required='false' scope='global' />

<@requirement.PARAM name='LS_JAVA_OPTS' value='-Xms1g -Xmx1g -Dnetworkaddress.cache.ttl=10' scope='global' />
<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<#if environment == 'HOTFIX' || environment == 'INTEGRATION' || environment == 'INTEGRATION_LP' || environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3' || environment == 'PRE-PROD' || environment == 'PRE-PROD_F' || environment == 'TESTUOT' || environment == 'LOAD'>

<#else>
  <#assign unknownEnvironment = true />
</#if>

<@requirement.PARAM name='BUFFER_PENDING_REJECT_TIMEOUT_MIN' required='false' value='30' />
<@requirement.PARAM name='BUFFER_ACTIVE_REJECT_TIMEOUT_MIN'  required='false' value='5' />
<@requirement.PARAM name='BUFFER_AUTO_CLOSE_TIMEOUT_DAYS'    required='false' value='1' />
<@requirement.PARAM name='POOLS_CHECKER_TIMEOUT_SEC'         required='false' value='30' />

<@requirement.PARAM name='POOLS_CHECKER_SCHEDULER_SIZE'   required='false' value='32' />
<@requirement.PARAM name='ORDERS_APPROVER_SCHEDULER_SIZE' required='false' value='16' />
<@requirement.PARAM name='BUFFERS_FILLER_SCHEDULER_SIZE'  required='false' value='32' />

<@swarm.SERVICE 'oms-${namespace}' 'equironpnz/oms-app:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.PROFILER_PORT '9955' 'host' />

  <@service.CONSTRAINT 'oms-checkqr' 'true' />
  <@service.LABEL 'oms' 'true' />

  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.NETWORK 'net-${PARAMS.OMS_ROUTER_NAMESPACE}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.ENV 'IN_MEMORY' PARAMS.OMS_IN_MEMORY />
  <@service.ENV 'MAX_CODES_IN_MEMORY_STORAGE_SIZE_MB' PARAMS.MAX_CODES_IN_MEMORY_STORAGE_SIZE_MB />
  <@service.ENV 'BUFFER_FORCE_OUT_TIMEOUT_MIN' PARAMS.BUFFER_FORCE_OUT_TIMEOUT_MIN />

  <@service.ENV 'postgresIp' 'postgres-${namespace}' />
  <@service.ENV 'postgresPort' '5432' />
  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.COUCHDB_PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-oms-${namespace}:4560' />

  <@service.ENV 'OMSLET_ID' PARAMS.OMSLET_ID />
  <@service.ENV 'UI_URL' 'http://oms-ui-${namespace}:3000' />

  <@service.ENV 'OMS_CLOUD_ENTITY_ID' PARAMS.OMS_CLOUD_ENTITY_ID />
  <@service.ENV 'REM_PROXY_TOKEN' PARAMS.REM_PROXY_TOKEN />
  <@service.ENV 'OMS_SERVER_URL' 'http://oms-server-${namespace}:9090/' />

  <@service.ENV 'SPRING_PROFILES_ACTIVE' 'TOBACCO,PHARMA,LIGHT,SHOES,MILK,TIRES,PHOTO,PERFUM,BICYCLE,WHEELCHAIRS,OTP${(PARAMS.STANDALONE == "true")?then(",STANDALONE", "")}' />

  <@service.ENV 'ENABLE_PROFILER' PARAMS.ENABLE_PROFILER />
  <@service.ENV 'OMS_WORKER_THREADS' PARAMS.OMS_WORKER_THREADS />
  <@service.ENV 'BUFFER_PENDING_REJECT_TIMEOUT_MIN' PARAMS.BUFFER_PENDING_REJECT_TIMEOUT_MIN />
  <@service.ENV 'BUFFER_ACTIVE_REJECT_TIMEOUT_MIN' PARAMS.BUFFER_ACTIVE_REJECT_TIMEOUT_MIN />
  <@service.ENV 'BUFFER_AUTO_CLOSE_TIMEOUT_DAYS' PARAMS.BUFFER_AUTO_CLOSE_TIMEOUT_DAYS />
  <@service.ENV 'POOLS_CHECKER_TIMEOUT_SEC' PARAMS.POOLS_CHECKER_TIMEOUT_SEC />
  <@service.ENV 'POOLS_CHECKER_SCHEDULER_SIZE' PARAMS.POOLS_CHECKER_SCHEDULER_SIZE />
  <@service.ENV 'ORDERS_APPROVER_SCHEDULER_SIZE' PARAMS.ORDERS_APPROVER_SCHEDULER_SIZE />
  <@service.ENV 'BUFFERS_FILLER_SCHEDULER_SIZE' PARAMS.BUFFERS_FILLER_SCHEDULER_SIZE />
  <@service.ENV 'BUFFER_SIZE' PARAMS.BUFFER_SIZE />
  <@service.ENV 'ENABLE_TEST_CERT' PARAMS.ENABLE_TEST_CERT />
  <@service.ENV 'CLOUD_OMS_NAME' 'OMS-${environment}-${namespace}' />
  
  <@service.ENV 'DISABLE_GIS_TRANSACTION' PARAMS.DISABLE_GIS_TRANSACTION />

  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' PARAMS.CENTRAL_DB_ENABLE_REPLICATION />
  <@service.ENV 'CENTRAL_DB_IP' PARAMS.CENTRAL_DB_IP />
  <@service.ENV 'CENTRAL_DB_PORT' PARAMS.CENTRAL_DB_PORT />
  <@service.ENV 'CENTRAL_DB_USER' PARAMS.CENTRAL_DB_USER />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.CENTRAL_DB_PASSWORD />

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'true' />
<#if unknownEnvironment??>
  <@service.ENV 'AUTO_APPROVE' PARAMS.OMS_AUTO_APPROVE />
  <@service.ENV 'OMS_HOME_URL' PARAMS.OMS_HOME_URL />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
</#if>

<#if environment == 'myserver'>

</#if>

<#if environment == 'PRE-PROD'>
  <@service.ENV 'AUTO_APPROVE' PARAMS.OMS_AUTO_APPROVE />
  <@service.ENV 'OMS_HOME_URL' 'https://omspreprod.crptech.ru:12001/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://it.mdlp.crpt.ru'/>
</#if>

<#if environment == 'LOAD'>
  <@service.ENV 'AUTO_APPROVE' 'true' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
</#if>

<#if environment == 'PRE-PROD_F'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://omspreprod.crptech.ru:12001/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
</#if>

<#if environment == 'HOTFIX'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://omshotfix.crptech.ru:12011/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://preprod.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/'/>

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  
  <@service.ENV 'TEST_MODE' 'true' />
</#if>

<#if environment == 'INTEGRATION'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://intpharma.crpt.ru:12001/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' '' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/'/>

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'INTEGRATION_LP'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://intuot.crpt.ru:12011/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://demo.lp.crpt.tech' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/'/>

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://sb.mdlp.crpt.ru'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'PROD'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://suz.crpt.ru/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' '' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/'/>
  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' ''/>
  <@service.ENV 'SETTINGS_ELK_URLS_MDLP_URL' 'https://mdlp.crpt.ru'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'PROD2'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://suz2.crpt.ru/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://prod.lp.crpt.tech' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/'/>
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'PROD3'>
  <@service.ENV 'AUTO_APPROVE' 'false' />
  <@service.ENV 'OMS_HOME_URL' 'https://suz3.crpt.ru/' />

  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_URL' 'https://prod.lp.crpt.tech' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/'/>
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>
  <@service.ENV 'SWAGGER_WEB_API' 'false'/>
</#if>

<#if environment == 'TESTUOT'>
  <@service.ENV 'AUTO_APPROVE' 'true' />
  <@service.ENV 'OMS_HOME_URL' 'http://testuot.crpt.ru:45676/' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />

  <@service.ENV 'CIPHER_PASSWORD' 'passpbqptc8vacw' />
  <@service.ENV 'CIPHER_SALT' 'saltpbqptc8vacw' />
</#if>

  <@service.CHECK_PATH ':8080/webapi/v1/version' />
</@swarm.SERVICE>

<@swarm.SERVICE 'oms-ui-${namespace}' 'registry.gitlab.com/equiron/oms-ui:1.13.10LP'>
  <@service.CONSTRAINT 'oms' 'true' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CHECK_PORT '3000' />
  <@service.ENV 'API_URL' 'http://oms-${namespace}:8080' />
  <@service.ENV 'BASE_URL' 'http://oms-${namespace}:8080' />
  <@service.ENV 'DISABLE_SIGNIN' PARAMS.DISABLE_SIGNIN />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-oms-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-oms-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'omsapp' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>
