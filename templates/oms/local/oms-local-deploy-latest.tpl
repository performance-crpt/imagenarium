<@requirement.CONSTRAINT 'oms' 'true' />

<@requirement.PARAM name='OMS_PUBLISHED_PORT' value='9999' required='false' type='port' scope='global' />
<@requirement.PARAM name='OMS_JAVA_OPTS' value='-Xmx16g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />

<@requirement.PARAM name='OMS_ID' scope='global' />
<@requirement.PARAM name='OMS_PASSWORD' type='password' scope='global' />

<@requirement.PARAM name='REMOTE_DB_IP' scope='global' />
<@requirement.PARAM name='REMOTE_DB_PORT' value='5984' scope='global' />

<@requirement.PARAM name='CENTRAL_DB_ENABLE_REPLICATION' value='false' type='boolean' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_IP' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PORT' value='5984' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_USER' value='admin' required='false' scope='global' />
<@requirement.PARAM name='CENTRAL_DB_PASSWORD' value='' type='password' required='false' scope='global' />

<@requirement.PARAM name='JMX_PORT' required='false' type='port' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-app' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' scope='global' />

<@requirement.PARAM name='BUFFER_PENDING_REJECT_TIMEOUT_MIN' required='false' value='30' />
<@requirement.PARAM name='BUFFER_ACTIVE_REJECT_TIMEOUT_MIN'  required='false' value='5' />
<@requirement.PARAM name='BUFFER_AUTO_CLOSE_TIMEOUT_DAYS'    required='false' value='1' />
<@requirement.PARAM name='POOLS_CHECKER_TIMEOUT_SEC'         required='false' value='30' />
<@requirement.PARAM name='ORDERS_PURGE_TIMEOUT_HOURS'        required='false' value='720' />
<@requirement.PARAM name='REPORTS_PURGE_TIMEOUT_HOURS'       required='false' value='720' />

<@requirement.PARAM name='POOLS_CHECKER_SCHEDULER_SIZE'   required='false' value='2' />
<@requirement.PARAM name='ORDERS_APPROVER_SCHEDULER_SIZE' required='false' value='1' />
<@requirement.PARAM name='BUFFERS_FILLER_SCHEDULER_SIZE'  required='false' value='1' />

<@requirement.PARAM name='DISABLE_GIS_TRANSACTION' value='false' type='boolean'/>
<@requirement.PARAM name='ENABLE_TEST_CERT' value='false' type='boolean' scope='global' />

<@requirement.PARAM name='OMS_LOCAL_ENTITY_ID' scope='global' required='false' />
<@requirement.PARAM name='REM_PROXY_TOKEN' scope='global' required='false' />

<@requirement.PARAM name='LS_JAVA_OPTS' value='-Xms512m -Xmx512m -Dnetworkaddress.cache.ttl=10' scope='global' />
<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<@swarm.SERVICE 'oms-ui-${namespace}' 'registry.gitlab.com/equiron/oms-ui:1.5.20'>
  <@service.CONSTRAINT 'oms' 'true' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CHECK_PORT '3000' />
  <@service.ENV 'API_URL' 'http://oms-${namespace}:8080' />
  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'false' />
</@swarm.SERVICE>

<@swarm.SERVICE 'oms-${namespace}' 'equironpnz/oms-app:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.OMS_PUBLISHED_PORT '8080' />
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />
  <@service.VOLUME '/tmp' />
  <@service.CONSTRAINT 'oms' 'true' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.STOP_GRACE_PERIOD '60s' />

  <@service.ENV 'OMSLET_ID' PARAMS.OMS_ID />
  <@service.ENV 'UI_URL' 'http://oms-ui-${namespace}:3000' />

  <@service.ENV 'postgresIp' 'postgres-${namespace}' />
  <@service.ENV 'postgresPort' '5432' />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />

  <@service.ENV 'LOGSTASH_HOST' 'logstash-oms-${namespace}:4560' />

  <@service.ENV 'JAVA_OPTS' PARAMS.OMS_JAVA_OPTS />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />

  <@service.ENV 'OMS_ID' PARAMS.OMS_ID />
  <@service.ENV 'OMS_PASSWORD' PARAMS.OMS_PASSWORD />

  <@service.ENV 'ENABLE_TEST_CERT' PARAMS.ENABLE_TEST_CERT />
  <@service.ENV 'DISABLE_GIS_TRANSACTION' PARAMS.DISABLE_GIS_TRANSACTION />

  <@service.ENV 'ENABLE_REPLICATION' 'true' />
  <@service.ENV 'REMOTE_DB_IP' PARAMS.REMOTE_DB_IP />
  <@service.ENV 'REMOTE_DB_PORT' PARAMS.REMOTE_DB_PORT />

  <@service.ENV 'CENTRAL_DB_ENABLE_REPLICATION' PARAMS.CENTRAL_DB_ENABLE_REPLICATION />
  <@service.ENV 'CENTRAL_DB_IP' PARAMS.CENTRAL_DB_IP />
  <@service.ENV 'CENTRAL_DB_PORT' PARAMS.CENTRAL_DB_PORT />
  <@service.ENV 'CENTRAL_DB_USER' PARAMS.CENTRAL_DB_USER />
  <@service.ENV 'CENTRAL_DB_PASSWORD' PARAMS.CENTRAL_DB_PASSWORD />

  <@service.ENV 'SPRING_PROFILES_ACTIVE' 'PHOTO,PERFUM,TOBACCO,LIGHT,PHARMA,MILK,TIRES,BICYCLE,WHEELCHAIRS,NOTCLOUD' />
  
  <@service.ENV 'BUFFER_PENDING_REJECT_TIMEOUT_MIN' PARAMS.BUFFER_PENDING_REJECT_TIMEOUT_MIN />
  <@service.ENV 'BUFFER_ACTIVE_REJECT_TIMEOUT_MIN' PARAMS.BUFFER_ACTIVE_REJECT_TIMEOUT_MIN />
  <@service.ENV 'BUFFER_AUTO_CLOSE_TIMEOUT_DAYS' PARAMS.BUFFER_AUTO_CLOSE_TIMEOUT_DAYS />
  <@service.ENV 'POOLS_CHECKER_TIMEOUT_SEC' PARAMS.POOLS_CHECKER_TIMEOUT_SEC />
  <@service.ENV 'ORDERS_PURGE_TIMEOUT_HOURS' PARAMS.ORDERS_PURGE_TIMEOUT_HOURS />
  <@service.ENV 'REPORTS_PURGE_TIMEOUT_HOURS' PARAMS.REPORTS_PURGE_TIMEOUT_HOURS />

  <@service.ENV 'BUFFER_SIZE' PARAMS.BUFFER_SIZE />
  <@service.ENV 'POOLS_CHECKER_SCHEDULER_SIZE' PARAMS.POOLS_CHECKER_SCHEDULER_SIZE />
  <@service.ENV 'ORDERS_APPROVER_SCHEDULER_SIZE' PARAMS.ORDERS_APPROVER_SCHEDULER_SIZE />
  <@service.ENV 'BUFFERS_FILLER_SCHEDULER_SIZE' PARAMS.BUFFERS_FILLER_SCHEDULER_SIZE />

  <@service.ENV 'OMS_LOCAL_ENTITY_ID' PARAMS.OMS_LOCAL_ENTITY_ID />
  <@service.ENV 'REM_PROXY_TOKEN' PARAMS.REM_PROXY_TOKEN />
  
  <@service.LABEL 'traefik.port' '8080' />
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '31457280' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '52428800‬' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '31457280' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '52428800‬' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/webapi/v1/version' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'oms-${namespace}' />
  <@service.LABEL 'traefik.enable' 'false' />

  <@service.CHECK_PATH ':8080/webapi/v1/version' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-oms-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-oms-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'omsapp' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>