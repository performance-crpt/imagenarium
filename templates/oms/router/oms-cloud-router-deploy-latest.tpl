<@requirement.CONSTRAINT 'traefik-oms' 'true' />

<@requirement.PARAM name='OMS_PUBLISHED_PROXY_PORT' type='port' value='9999' scope='global' />
<@requirement.PARAM name='OMS_PUBLISHED_PROXY_HTTPS_PORT' type='port' value='9443' required='false' scope='global' />
<@requirement.PARAM name='OMS_PUBLISHED_PROXY_ADMIN_PORT' type='port' value='9998' required='false' scope='global' />

<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-router' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />

<#if environment == 'HOTFIX' || environment == 'INTEGRATION' || environment == 'INTEGRATION_LP' || environment == 'PROD' || environment == 'PRE-PROD' || environment == 'PRE-PROD_F' || environment == 'PROD2' || environment == 'PROD3' || environment == 'LOAD'>
  <#assign enableHttps = true />
<#else>
  <#assign enableHttps = false />
</#if>

<@swarm.SERVICE 'traefik-oms-${namespace}' 'registry.gitlab.com/equiron/sitemanager/traefik:1.7-alpine' "--logLevel=INFO --metrics.prometheus --docker --docker.swarmMode --docker.watch --api --constraints='tag==oms-${namespace}'" 'global'>
  <@node.MANAGER />
  <@service.DNSRR />
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'traefik-oms' 'true' />
  <@service.PORT PARAMS.OMS_PUBLISHED_PROXY_PORT '80' 'host' />
  <@service.PORT PARAMS.OMS_PUBLISHED_PROXY_HTTPS_PORT '443' 'host' />
  <@service.PORT PARAMS.OMS_PUBLISHED_PROXY_ADMIN_PORT '8080' 'host' />
  <@service.CHECK_PATH ':8080/dashboard/' />
  <@service.ENV 'METRICS_ENDPOINT' ':8080/metrics' />
</@swarm.SERVICE>

<@swarm.SERVICE 'oms-router-${namespace}' 'equironpnz/oms-router:${PARAMS.TAG}' '' 'global'>
  <@service.CONSTRAINT 'traefik-oms' 'true' />
  <@node.MANAGER />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />  
  <@service.DNSRR />  
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.CHECK_PORT '8811' />
  
  <#if enableHttps>
    <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
    <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
    <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
  <#else>
    <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
  </#if>

  <@service.LABEL 'traefik.port' '8811' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '31457280' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '52428800' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '31457280' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '52428800' />
  <@service.LABEL 'traefik.tags' 'oms-${namespace}' />
  <@service.LABEL 'traefik.backend' 'oms' />
</@swarm.SERVICE>
