<@requirement.CONSTRAINT 'oms-server' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='COUCHDB_PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true -Dcom.sun.security.enableCRLDP=true -Docsp.enable=true' scope='global' />
<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-server' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />
<@requirement.PARAM name='ENABLE_HTTPS' value='true' type='boolean' scope='global' />
<@requirement.PARAM name='SETTINGS_SIGNIN_DISABLE_SIGNIN' value='true' type='boolean' scope='global' />

<@requirement.PARAM name='ORDERS_PURGE_TIMEOUT_HOURS' required='false' value='720' />
<@requirement.PARAM name='REPORTS_PURGE_TIMEOUT_HOURS' required='false' value='720' />

<@requirement.PARAM name='OMS_SERVER_ENTITY_ID' scope='global' required='false' />
<@requirement.PARAM name='OMSLET_ID' scope='global' required='true' />
<@requirement.PARAM name='SERVER_SERIAL_5DIGITS' scope='global'/>

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<#if environment == 'HOTFIX' || environment == 'INTEGRATION' || environment == 'INTEGRATION_LP' || environment == 'PROD' || environment == 'PROD2' || environment == 'PROD3' || environment == 'PRE-PROD' || environment == 'TESTUOT'>

<#else>
  <#assign unknownEnvironment = true />
</#if>

<@swarm.SERVICE 'oms-server-${namespace}' 'equironpnz/oms-server:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.CONSTRAINT 'oms-server' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />

  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.COUCHDB_PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-server-${namespace}:4560' />
  <@service.ENV 'OMS_CLOUD_URL' 'http://oms-${namespace}:8080/' />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
  <@service.ENV 'SERVER_NAME' 'SERVER-${environment}-${namespace}' />

  <@service.ENV 'ORDERS_PURGE_TIMEOUT_HOURS' PARAMS.ORDERS_PURGE_TIMEOUT_HOURS />
  <@service.ENV 'REPORTS_PURGE_TIMEOUT_HOURS' PARAMS.REPORTS_PURGE_TIMEOUT_HOURS />

  <@service.ENV 'OMS_SERVER_ENTITY_ID' PARAMS.OMS_SERVER_ENTITY_ID />
  <@service.ENV 'OMSLET_ID' PARAMS.OMSLET_ID />

<#if unknownEnvironment??>
  <@service.ENV 'OMS_HOME_URL' 'http://91.242.38.69:12908' />
  <@service.ENV 'SERVER_HOME_URL' 'http://91.242.38.69:12908/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' PARAMS.SERVER_SERIAL_5DIGITS />
  <@service.ENV 'AUTO_APPROVE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_BILLING_DISABLE' 'true' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_DISABLE'   'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' PARAMS.SETTINGS_SIGNIN_DISABLE_SIGNIN />
</#if>

<#if environment == 'TESTUOT'>
  <@service.ENV 'OMS_HOME_URL' 'http://testuot.crpt.ru:45676' />
  <@service.ENV 'SERVER_HOME_URL' 'http://testuot.crpt.ru:45679/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' PARAMS.SERVER_SERIAL_5DIGITS />
  <@service.ENV 'AUTO_APPROVE' 'true' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_DISABLE'   'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'false' />
</#if>

<#if environment == 'PRE-PROD'>
  <@service.ENV 'OMS_HOME_URL' 'https://omspreprod.crptech.ru:12001' />
  <@service.ENV 'SERVER_HOME_URL' 'https://omspreprod.crptech.ru:12005/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' PARAMS.SERVER_SERIAL_5DIGITS />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://router.int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'documents/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'documents/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'documents/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://preprod.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://doc-router-lp.apps.preprod.lp.crpt.tech/api/v3/external-api/' />
  <@service.ENV 'SETTINGS_IS_MP_AGGREGATION_PATH' 'aggregation/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://sb.mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'false' />
</#if>

<#if environment == 'HOTFIX'>
  <@service.ENV 'OMS_HOME_URL' 'https://omshotfix.crptech.ru:12011' />
  <@service.ENV 'SERVER_HOME_URL' 'https://omshotfix.crptech.ru:12015/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' PARAMS.SERVER_SERIAL_5DIGITS />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://router.int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'documents/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'documents/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'documents/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://preprod.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://preprod.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://doc-router-lp.apps.preprod.lp.crpt.tech/api/v3/external-api/' />
  <@service.ENV 'SETTINGS_IS_MP_AGGREGATION_PATH' 'aggregation/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://sb.mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://qa02.gismt.crpt.tech/'/>

  <@service.ENV 'TEST_MODE' 'true' />
</#if>

<#if environment == 'INTEGRATION'>
  <@service.ENV 'OMS_HOME_URL' 'https://intpharma.crpt.ru:12001' />
  <@service.ENV 'SERVER_HOME_URL' 'https://intpharma.crpt.ru:12005/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' PARAMS.SERVER_SERIAL_5DIGITS />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://router.int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'documents/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'documents/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'documents/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://demo.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://doc-router-lp.apps.preprod.lp.crpt.tech/api/v3/external-api/' />
  <@service.ENV 'SETTINGS_IS_MP_AGGREGATION_PATH' 'aggregation/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://sb.mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' ''/>

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' PARAMS.SETTINGS_SIGNIN_DISABLE_SIGNIN />
</#if>

<#if environment == 'INTEGRATION_LP'>
  <@service.ENV 'OMS_HOME_URL' 'https://intuot.crpt.ru:12011' />
  <@service.ENV 'SERVER_HOME_URL' 'https://intuot.crpt.ru:12015/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' PARAMS.SERVER_SERIAL_5DIGITS />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://router.int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://int01.gismt.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'documents/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'documents/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'documents/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://demo.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://demo.lp.crpt.tech/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'NfyIb{fq' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'change_me' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://doc-router-lp.apps.preprod.lp.crpt.tech/api/v3/external-api/' />
  <@service.ENV 'SETTINGS_IS_MP_AGGREGATION_PATH' 'aggregation/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://sb.mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://api.sb.mdlp.crpt.ru/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.demo.crpt.tech/'/>

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' PARAMS.SETTINGS_SIGNIN_DISABLE_SIGNIN />
</#if>

<#if environment == 'PROD'>
  <@service.ENV 'OMS_HOME_URL' 'https://suz.crpt.ru' />
  <@service.ENV 'SERVER_HOME_URL' 'https://suzsrv.crpt.ru/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' '00001' />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://stable.ismotp.crptech.ru/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://stable.ismotp.crptech.ru/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'api/v3/facade/suz/reports/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'api/v3/facade/suz/codes/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'api/v3/receiver-api/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://ismp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://router.prod01.rtng.crpt.tech/documents/' />
  <@service.ENV 'SETTINGS_IS_MP_AGGREGATION_PATH' 'aggregation/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://192.168.51.66/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'true' />
</#if>

<#if environment == 'PROD2'>
  <@service.ENV 'SERVER_HOME_URL' 'https://suz2.crpt.ru' />
  <@service.ENV 'SERVER_HOME_URL' 'https://suzsrv2.crpt.ru/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' '00002' />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://stable.ismotp.crptech.ru/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://stable.ismotp.crptech.ru/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'api/v3/facade/suz/reports/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'api/v3/facade/suz/codes/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'api/v3/receiver-api/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://ismp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://router.prod01.rtng.crpt.tech/documents/' />
  <@service.ENV 'SETTINGS_IS_MP_AGGREGATION_PATH' 'aggregation/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://192.168.51.66/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'true' />
</#if>

<#if environment == 'PROD3'>
  <@service.ENV 'OMS_HOME_URL' 'https://suz3.crpt.ru' />
  <@service.ENV 'SERVER_HOME_URL' 'https://suzsrv3.crpt.ru/' />
  <@service.ENV 'SERVER_SERIAL_5DIGITS' '00002' />
  <@service.ENV 'AUTO_APPROVE' 'false' />

  <@service.ENV 'SETTINGS_IS_MOTP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MOTP_URL' 'https://stable.ismotp.crptech.ru/' />
  <@service.ENV 'SETTINGS_IS_MOTP_TOKEN_URL' 'https://stable.ismotp.crptech.ru/' />
  <@service.ENV 'SETTINGS_IS_MOTP_AGGREGATION_PATH' 'api/v3/facade/suz/reports/aggregation/' />
  <@service.ENV 'SETTINGS_IS_MOTP_DROPOUT_PATH' 'api/v3/facade/suz/codes/dropped_out/' />
  <@service.ENV 'SETTINGS_IS_MOTP_SIGNED_REPORT_PATH' 'api/v3/receiver-api/tc/' />

  <@service.ENV 'SETTINGS_IS_MP_DISABLE' 'false' />
  <@service.ENV 'SETTINGS_IS_MP_URL' 'https://ismp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MP_TOKEN_URL' 'https://ismp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MP_NAME' 'admin' />
  <@service.ENV 'SETTINGS_IS_MP_PASSWORD' 'cradminpt' />
  <@service.ENV 'SETTINGS_IS_MP_DOC_ROUTER_URL' 'https://router.prod01.rtng.crpt.tech/documents/' />
  <@service.ENV 'SETTINGS_IS_MP_DROPOUT_PATH' 'dropped_out/' />

  <@service.ENV 'SETTINGS_IS_MDLP_DISABLE' 'true' />
  <@service.ENV 'SETTINGS_IS_MDLP_URL' 'https://mdlp.crpt.ru/' />
  <@service.ENV 'SETTINGS_IS_MDLP_API_URL' 'http://192.168.51.66/' />

  <@service.ENV 'CRPT_AUTH_URL' '' />
  <@service.ENV 'JWT_SECRET_KEY' '' />

  <@service.ENV 'SETTINGS_ELK_URLS_ELK_URL' 'https://markirovka.crpt.ru/'/>

  <@service.ENV 'SETTINGS_SIGNIN_DISABLE_SIGNIN' 'true' />
</#if>

<#if PARAMS.ENABLE_HTTPS == 'true'>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http,https' />
  <@service.LABEL 'traefik.frontend.headers.SSLRedirect' 'true' />
  <@service.LABEL 'traefik.frontend.redirect.entryPoint' 'https' />
<#else>
  <@service.LABEL 'traefik.frontend.entryPoints' 'http' />
</#if>

  <@service.LABEL 'traefik.port' '9090' />
  <@service.LABEL 'traefik.frontend.rule' 'PathPrefix:/' />
  <@service.LABEL 'traefik.docker.network' 'net-${namespace}' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness' 'true' />
  <@service.LABEL 'traefik.backend.loadbalancer.stickiness.cookieName' 'targetsrv' />
  <@service.LABEL 'traefik.backend.loadbalancer.method' 'drr' />
  <@service.LABEL 'traefik.backend.buffering.maxRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.maxResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memRequestBodyBytes'  '10485760' />
  <@service.LABEL 'traefik.backend.buffering.memResponseBodyBytes' '10485760' />
  <@service.LABEL 'traefik.backend.healthcheck.path' '/version' />
  <@service.LABEL 'traefik.backend.healthcheck.interval' '10s' />
  <@service.LABEL 'traefik.tags' 'oms-server-${namespace}' />

  <@service.CHECK_PATH ':9090/version' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-server-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-server' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-server-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'server' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>