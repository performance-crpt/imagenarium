<@requirement.CONSTRAINT 'oms-funds' 'true' />

<@requirement.PARAM name='FUNDS_PUBLISHED_PORT' type='port' scope='global' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />
<@requirement.PARAM name='BROKER_KAFKA' scope='global' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-funds' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />
<@requirement.PARAM name='EXTERNAL_STORE_URL' scope='global' />

<@requirement.PARAM name='ONYMA_ENABLED' value='false' type='boolean' scope='global' />

<@swarm.SERVICE 'oms-funds-${namespace}' 'equironpnz/oms-funds:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}'>
  <@service.PORT PARAMS.FUNDS_PUBLISHED_PORT '10080' 'host' />
  <@service.CONSTRAINT 'oms-funds' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />

  <@service.ENV 'UI_URL' 'http://oms-funds-ui-${namespace}:3000' />
  
  <@service.ENV 'ONYMA_ENABLED' PARAMS.ONYMA_ENABLED />
  <@service.ENV 'postgresIp' 'postgres-${namespace}' />
  <@service.ENV 'postgresPort' '5432' />
  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-funds-${namespace}:4560' />
  
  <@service.ENV 'brokerList' PARAMS.BROKER_KAFKA />
  
  <@service.ENV 'EXTERNAL_STORE_URL' PARAMS.EXTERNAL_STORE_URL />
  <@service.ENV 'EXTERNAL_DOCUMENT_API_URL' 'http://document-api-${namespace}:8080' />
    
</@swarm.SERVICE>

<@swarm.SERVICE 'oms-funds-ui-${namespace}' 'registry.gitlab.com/equiron/oms-funds-ui/frontnend:1.2.38'>
  <@service.CONSTRAINT 'oms-funds' 'true' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CHECK_PORT '3000' />
  <@service.ENV 'SERVER_PORT' '3000' />
  <@service.ENV 'API_URL' 'http://oms-funds-${namespace}:10080' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-funds-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-funds' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-funds-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'omsfunds' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>
