<@requirement.CONSTRAINT 'oms-transaction-server' 'true' />

<@requirement.PARAM name='COUCHDB_PASSWORD' type='password' scope='global' />
<@requirement.PARAM name='PROXY_BIND_PORT' values='80,5984' value='80' type='select' required='false' />

<@requirement.PARAM name='JAVA_OPTS' value='-Xmx8g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' value='latest-master' type='tag' filter='oms-transaction-server' />
<@requirement.PARAM name='TAG_OVERRIDE' required='false' />

<@requirement.PARAM name='OMS_GISTRANSACTION_SERVER_ENTITY_ID'       scope='global' required='false' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' scope='global' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' scope='global' />
<@requirement.PARAM name='DELETE_DAYS' value='30' scope='global' />

<@requirement.PARAM name='EXTERNAL_STORE_URL' scope='global' />

<@requirement.PARAM name='BROKER_KAFKA' scope='global' />
<@requirement.PARAM name='KAFKA_TOPIC_RESULT' scope='global' />
<@requirement.PARAM name='KAFKA_TOPIC_BUFFER' scope='global' />
<@requirement.PARAM name='KAFKA_TOPIC_EVENTS' scope='global' />
<@requirement.PARAM name='KAFKA_TOPIC_NOTIFICATION_DOCS' scope='global' />

<@swarm.SERVICE 'oms-transaction-server-${namespace}' 'equironpnz/oms-transaction-server:${PARAMS.TAG_OVERRIDE?has_content?then(PARAMS.TAG_OVERRIDE, PARAMS.TAG)}' '' 'global'>
  <@service.PORT PARAMS.JMX_PORT PARAMS.JMX_PORT />

  <@service.CONSTRAINT 'oms-transaction-server' 'true' />
  <@service.VOLUME '/tmp' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.STOP_GRACE_PERIOD '60s' />
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />

  <@service.ENV 'postgresIp' 'postgres-${namespace}' />
  <@service.ENV 'postgresPort' '5432' />
  <@service.ENV 'couchDbIp' 'couchdb-${namespace}' />
  <@service.ENV 'couchDbPort' PARAMS.PROXY_BIND_PORT />
  <@service.ENV 'couchDbPassword' PARAMS.COUCHDB_PASSWORD />
  <@service.ENV 'LOGSTASH_HOST' 'logstash-transactionserver-${namespace}:4560' />
  <@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
  <@service.ENV 'GISTRANSACTION_SERVER_NAME' 'GISTRANSACTION_SERVER-${environment}-${namespace}' />
  <@service.ENV 'EXTERNAL_STORE_URL' PARAMS.EXTERNAL_STORE_URL />
  <@service.ENV 'EXTERNAL_DOCUMENT_API_URL' 'http://document-api-${namespace}:9087' />

  <@service.ENV 'brokerList' PARAMS.BROKER_KAFKA />
  <@service.ENV 'KAFKA_TOPIC_RESULT' PARAMS.KAFKA_TOPIC_RESULT />
  <@service.ENV 'KAFKA_TOPIC_BUFFER' PARAMS.KAFKA_TOPIC_BUFFER />
  <@service.ENV 'KAFKA_TOPIC_EVENTS' PARAMS.KAFKA_TOPIC_EVENTS />
  <@service.ENV 'KAFKA_TOPIC_NOTIFICATION_DOCS' PARAMS.KAFKA_TOPIC_NOTIFICATION_DOCS />
  
  <@service.ENV 'OMS_GISTRANSACTION_SERVER_ENTITY_ID' PARAMS.OMS_GISTRANSACTION_SERVER_ENTITY_ID />

</@swarm.SERVICE>


<@swarm.SERVICE 'document-api-${namespace}' 'registry.gitlab.com/equiron/sitemanager/document-api:0.0.5'>
  <@service.CONSTRAINT 'oms-transaction-server' 'true' />
  <@service.PORT '9087' '8080' 'host' />
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CHECK_PORT '8080' />
</@swarm.SERVICE>

<#if PARAMS.ES_URL?has_content>
  <@swarm.SERVICE 'logstash-transactionserver-${namespace}' 'imagenarium/logstash:7.5.0'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'oms-transaction-server' 'true' />
    <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL />
    <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
    <@service.ENV 'NODE_NAME' 'logstash-transactionserver-${namespace}' />
    <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
    <@service.ENV 'INDEX_NAME' 'transactionserver' />
    <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
    <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
    <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
    <@service.CHECK_PORT '4560' />
  </@swarm.SERVICE>
</#if>
