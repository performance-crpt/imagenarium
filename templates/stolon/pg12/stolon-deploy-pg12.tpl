<@requirement.CONSTRAINT 'stolon' '1' />
<@requirement.CONSTRAINT 'stolon' '2' />
<@requirement.CONSTRAINT 'stolon' '3' />
<@requirement.CONSTRAINT 'stolon-proxy' 'true' />

<@requirement.PARAM name='PUBLISHED_PORT' type='port' required='false' description='Specify postgres external port (for example 5432)' />
<@requirement.PARAM name='POSTGRES_DB' value='postgres' />
<@requirement.PARAM name='POSTGRES_USER' value='postgres' />
<@requirement.PARAM name='POSTGRES_PASSWORD' value='postgres' />
<@requirement.PARAM name='POSTGRES_SHARED_BUFFERS' value='128MB' />
<@requirement.PARAM name='POSTGRES_WAL_KEEP_SEGMENTS' value='10000' />
<@requirement.PARAM name='POSTGRES_MAX_CONNECTIONS' value='2000' />
<@requirement.PARAM name='POSTGRES_PARAMS' value='\"effective_io_concurrency\":\"10\"' type='textarea' />
<@requirement.PARAM name='APP_USER' value='app' />
<@requirement.PARAM name='APP_PASSWORD' value='app' />
<@requirement.PARAM name='PMM_ENABLE' value='false' type='boolean' />

<#assign STOLON_VERSION = 'pg12' />

<#if PARAMS.DELETE_DATA == 'true'>
  <@docker.CONTAINER 'stolon-init-${namespace}' 'imagenarium/stolon:${STOLON_VERSION}'>
    <@container.NETWORK 'net-${namespace}' />
    <@container.ENV 'ROLE' 'INIT' />
    <@container.ENV 'SHARED_BUFFERS' PARAMS.POSTGRES_SHARED_BUFFERS />
    <@container.ENV 'WAL_KEEP_SEGMENTS' PARAMS.POSTGRES_WAL_KEEP_SEGMENTS />
    <@container.ENV 'MAX_CONNECTIONS' PARAMS.POSTGRES_MAX_CONNECTIONS />
    <@container.ENV 'PG_PARAMS' PARAMS.POSTGRES_PARAMS />
    <@container.EPHEMERAL />
  </@docker.CONTAINER>
</#if>

<#list 1..3 as index>
  <@swarm.SERVICE 'stolon-sentinel-${index}-${namespace}' 'imagenarium/stolon:${STOLON_VERSION}'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.DNSRR />
    <@service.CONSTRAINT 'stolon' '${index}' />
    <@service.ENV 'ROLE' 'SENTINEL' />
    <@service.CHECK_PORT '8585' />
  </@swarm.SERVICE>
</#list>

<#list 1..3 as index>
  <@img.TASK 'stolon-keeper-${index}-${namespace}' 'imagenarium/stolon:${STOLON_VERSION}'>
    <@img.NETWORK 'net-${namespace}' />
    <@img.DNSRR />
    <@img.VOLUME '/var/lib/postgresql/data' />
    <@img.BIND '/sys/kernel/mm/transparent_hugepage' '/tph' />
    <@img.BIND '/dev/shm' '/dev/shm' />
    <@img.CONSTRAINT 'stolon' '${index}' />
    <@img.ENV 'ROLE' 'KEEPER' />
    <@img.ENV 'KEEPER_ID' '${index}' />
    <@img.ENV 'POSTGRES_USER' PARAMS.POSTGRES_USER />
    <@img.ENV 'POSTGRES_PASSWORD' PARAMS.POSTGRES_PASSWORD />
    <@img.ENV 'NETWORK_NAME' 'net-${namespace}' />
    <@img.ENV 'PMM' PARAMS.PMM_ENABLE />
  </@img.TASK>
</#list>

<@docker.CONTAINER 'stolon-patch-${namespace}' 'imagenarium/stolon:${STOLON_VERSION}'>
  <@container.NETWORK 'net-${namespace}' />
  <@container.ENV 'ROLE' 'PATCH' />
  <@container.ENV 'SHARED_BUFFERS' PARAMS.POSTGRES_SHARED_BUFFERS />
  <@container.ENV 'WAL_KEEP_SEGMENTS' PARAMS.POSTGRES_WAL_KEEP_SEGMENTS />
  <@container.ENV 'MAX_CONNECTIONS' PARAMS.POSTGRES_MAX_CONNECTIONS />
  <@container.ENV 'PG_PARAMS' PARAMS.POSTGRES_PARAMS />
  <@container.EPHEMERAL />
</@docker.CONTAINER>

<@swarm.SERVICE 'postgres-${namespace}' 'imagenarium/stolon:${STOLON_VERSION}'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.PORT PARAMS.PUBLISHED_PORT '5432' 'host' />
  <@service.VOLUME '/tmp' />
  <@service.CONSTRAINT 'stolon-proxy' 'true' />
  <@service.ENV 'POSTGRES_DB' PARAMS.POSTGRES_DB />
  <@service.ENV 'POSTGRES_USER' PARAMS.POSTGRES_USER />
  <@service.ENV 'POSTGRES_PASSWORD' PARAMS.POSTGRES_PASSWORD />
  <@service.ENV 'APP_USER' PARAMS.APP_USER />
  <@service.ENV 'APP_PASSWORD' PARAMS.APP_PASSWORD />
  <@service.ENV 'ROLE' 'PROXY' />
  <@service.CHECK_PORT '5432' />
</@swarm.SERVICE>
