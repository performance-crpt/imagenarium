#!/bin/bash

set -e

docker build --no-cache --squash -t ovarb6/wiremock:2.35 .
docker push ovarb6/wiremock:2.35
